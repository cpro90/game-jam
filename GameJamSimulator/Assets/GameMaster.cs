﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMaster : MonoBehaviour {

	public static GameMaster instance;

    public Text moneyText, projectPercentText, projectQualityText;
    public Image projectPercentImage, projectQualityImage, rufPercentImage;

	public Project project;

    public int money;
    public float ruf;

	void Awake(){
		instance = this;
	}

    public Stats roomStat;
    public Stats[] tableStats;

	// Use this for initialization
	void Start () {
		money = 1000;
		project = new Project();
		ruf = 50;	
	}

	// Update is called once per frame
	void Update () {
        
        moneyText.text = money.ToString();
		float projectClamp = Mathf.Clamp01(project.completed);
        projectPercentText.text = (projectClamp * 100).ToString("#0\\%");
        projectPercentImage.fillAmount = projectClamp;
		float projectQualityClamp = Mathf.Clamp01(project.quality);
		projectQualityText.text = (((projectQualityClamp+1.0f)/2.0f) * 100).ToString("#0\\%");
		projectQualityImage.fillAmount = (projectQualityClamp+1.0f)/2.0f;
        rufPercentImage.fillAmount = Mathf.Clamp01(ruf);


        roomStat = new Stats(0, 0, 0);
        foreach (ObjectPlace place in ObjectPlace.allMedium) {
            if(place.objectOnMe != null) {
                roomStat = Stats.Add(roomStat, place.objectOnMe.GetGlobalStatModifier());
            }
        }
        foreach (ObjectPlace place in ObjectPlace.allBig) {
            if (place.objectOnMe != null) {
                roomStat = Stats.Add(roomStat, place.objectOnMe.GetGlobalStatModifier());
            }
        }

        tableStats = new Stats[6];

        for (int i = 0; i < tableStats.Length; i++) {
            tableStats[i] = new Stats(0, 0, 0);
        }
        foreach (ObjectPlace place in ObjectPlace.allMedium) {
            if (place.objectOnMe != null && place.onTable) {
                tableStats[place.tableNr] = Stats.Add(tableStats[place.tableNr], place.objectOnMe.GetLocalStatModifier());
            }
        }
        foreach (ObjectPlace place in ObjectPlace.allBig) {
            if (place.objectOnMe != null && place.onTable) {
                tableStats[place.tableNr] = Stats.Add(tableStats[place.tableNr], place.objectOnMe.GetLocalStatModifier());
            }
        }
    }

    public void ShowMediumPlaces() {
        foreach (ObjectPlace place in ObjectPlace.allMedium) {
            place.gameObject.SetActive(true);
        }
    }

    public void ShowBigPlaces() {
        foreach (ObjectPlace place in ObjectPlace.allBig) {
            place.gameObject.SetActive(true);
        }
    }

}
