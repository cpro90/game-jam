﻿using UnityEngine;
using System.Collections;

public class LayerModifier : MonoBehaviour {


    private SpriteRenderer rend;
	// Use this for initialization
	void Start () {
        rend = GetComponent<SpriteRenderer>();

    }
	
	// Update is called once per frame
	void Update () {
        rend.sortingOrder = -(int)(transform.position.y * 10);

    }
}
