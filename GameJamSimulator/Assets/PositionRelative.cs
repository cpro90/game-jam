﻿using UnityEngine;
using System.Collections;

public class PositionRelative : MonoBehaviour {

    public GameObject relativeTo;
    public Vector3 offset;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = relativeTo.transform.position + offset;
	}
}
