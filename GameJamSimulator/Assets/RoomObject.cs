﻿using UnityEngine;
using System.Collections;

public class RoomObject : MonoBehaviour {

    public ObjectPlace standOn;
    
    public void MoveTo(ObjectPlace standOn) {

        transform.position = standOn.transform.position;

    }

    public virtual Stats GetLocalStatModifier() {
        return new Stats(0,0,0);
    }

    public virtual Stats GetGlobalStatModifier() {
        return new Stats(0, 0, 0);
    }

    public virtual void StartInteraction(Character2D character, Stats effectiveStats, out float interactionTime) {
        interactionTime = 5;
    }
    public virtual void HandleInteraction(Character2D character, Stats effectiveStats) {

    }
    public virtual void HandleInteractionEnd(Character2D character, Stats effectiveStats) {

    }
}
