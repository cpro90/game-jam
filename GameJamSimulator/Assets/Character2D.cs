﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Character2D : MonoBehaviour {

    public enum LookDirection { top, left, right, bottom };
    public enum Action { idle, move, interact };
    public RoomObject interactingWith;
    public RoomObject movingTo;

    public LookDirection lookDirection;
    public Action action;
    private int animationNr;
    private float animationTime;
    public float animationStayTime;

    public CharacterAnimation charImages;

    public SpriteRenderer moveTop, moveRight, moveLeft, moveBottom;

    public GameObject target;
    public Image bar;

    public float moodToShow;

    public AnimationCurve redToYellow, yellowToGreen;

    public AstarAI walkAI;
    private float lastLookChange;

	public int id;
	public int payment;

	public Stats tendency;
	public Stats current;

	static Stats getStandardTendency() {
		return new Stats (0.5f, 0f, 0f);
	}
    private float nextTimeActionQuery;

    public Stats effectiveStats;
	// Use this for initialization
	void Start () {
		payment = 10;
		tendency = getStandardTendency();
		current = tendency.copy();
    }

	public void work() {
		GameMaster.instance.money -= payment;
		Stats calculated = Stats.Add(Stats.Add(current, GameMaster.instance.roomStat),GameMaster.instance.tableStats[id]); //Calculate a stat of tendency, current stats and global stats

		GameMaster.instance.project.workOnProject(calculated);
		current.health -= calculated.motivation;
	}

	private const float stepper = 100; //stepper dann der deltaTime anpassen

	//Bewege current in Richtung Tendency
	private void calculateNewCurrent() {
		this.current.creativity -= (this.current.creativity - this.tendency.creativity) / stepper;
		this.current.motivation -= (this.current.motivation - this.tendency.motivation) / stepper;
		this.current.health -= (this.current.health - this.tendency.health) / stepper;
	}



    // Update is called once per frame
    void Update() {

        CalculateEffectiveStats();

        if (walkAI.walking)
            action = Action.move;
        else if (action == Action.move)
            HandleEndMove();

        if (walkAI.walking) {
            Vector3 directionWalking = walkAI.lastDirection;
            float x = Mathf.Abs(directionWalking.x);
            float y = Mathf.Abs(directionWalking.y);
            if (Time.time - lastLookChange > 0.2f) {
                if (x > y) {
                    lookDirection = directionWalking.x > 0 ? LookDirection.right : LookDirection.left;
                } else {
                    lookDirection = directionWalking.y > 0 ? LookDirection.top : LookDirection.bottom;
                }
                lastLookChange = Time.time;
            }
        }

        Animate();
        UpdateBar();

        if (interactingWith != null) {


            interactingWith.HandleInteraction(this, effectiveStats);

            if (Time.time > nextTimeActionQuery) {
                interactingWith.HandleInteractionEnd(this, effectiveStats);
                //TODO: neue aktion suchen
            }
        }
        else if (action == Action.idle && Time.time > nextTimeActionQuery) {
            //TODO: neue aktion suchen
        }
    }

    private void CalculateEffectiveStats() {
    }

    private void HandleEndMove() {
        interactingWith = movingTo;

        if (interactingWith != null) {
            action = Action.interact;
            float interactionTime;
            interactingWith.StartInteraction(this, effectiveStats, out interactionTime);
            nextTimeActionQuery = Time.time + interactionTime;
        } else {
            action = Action.idle;
            nextTimeActionQuery = Time.time + 5;
        }

    }

    private void Animate() {
        Sprite[] animationSource = null;
        SpriteRenderer imageToUse = null;
        switch (lookDirection) {
            case LookDirection.top:
            imageToUse = moveTop;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleUp;
                break;
                case Action.move:
                animationSource = charImages.moveUp;
                break;
                default:
                break;
            }
            break;
            case LookDirection.left:
            imageToUse = moveLeft;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleSide;
                break;
                case Action.move:
                animationSource = charImages.moveSide;
                break;
                default:
                break;
            }
            break;
            case LookDirection.right:
            imageToUse = moveRight;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleSide;
                break;
                case Action.move:
                animationSource = charImages.moveSide;
                break;
                default:
                break;
            }
            break;
            case LookDirection.bottom:
            imageToUse = moveBottom;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleDown;
                break;
                case Action.move:
                animationSource = charImages.moveDown;
                break;
                default:
                break;
            }
            break;
            default:
            break;
        }
        if(imageToUse == null || animationSource == null) {
            Debug.LogError("Wrong state!");
            return;
        }

        moveTop.gameObject.SetActive(moveTop == imageToUse);
        moveBottom.gameObject.SetActive(moveBottom == imageToUse);
        moveLeft.gameObject.SetActive(moveLeft == imageToUse);
        moveRight.gameObject.SetActive(moveRight == imageToUse);


        animationTime += Time.deltaTime;
        if(animationTime > animationStayTime) {
            animationTime = 0;
            animationNr++;
        }

        if (animationNr >= animationSource.Length) {
            animationNr = 0;
        }

        imageToUse.sprite = animationSource[animationNr];


    }

    public void UpdateBar() {
        bar.fillAmount = moodToShow;
        if (moodToShow <= 0.5)
            bar.color = Color.Lerp(Color.red, Color.yellow, redToYellow.Evaluate( moodToShow * 2));
        else
            bar.color = Color.Lerp(Color.yellow, Color.green, yellowToGreen.Evaluate( 2*(moodToShow -0.5f)));


    }
}
