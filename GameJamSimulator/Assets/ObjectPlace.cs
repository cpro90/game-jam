﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPlace : MonoBehaviour {

    public static List<ObjectPlace> allMedium = new List<ObjectPlace>();
    public static List<ObjectPlace> allBig = new List<ObjectPlace>();

    public RoomObject objectOnMe;
    public bool isMedium = true;
    public bool onTable;
    public int tableNr;


    // Use this for initialization
    void Start () {
        if (isMedium)
            allMedium.Add(this);
        else
            allBig.Add(this);

        gameObject.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
