﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Character : MonoBehaviour {

    public enum LookDirection { top, left, right, bottom };
    public enum Action { idle, move };

    public LookDirection lookDirection;
    public Action action;
    private int animationNr;
    private float animationTime;
    public float animationStayTime;

    public CharacterAnimation charImages;

    public Image moveTop, moveRight, moveLeft, moveBottom;

    public GameObject target;
    public Image bar;

    public float moodToShow;

    public AnimationCurve redToYellow, yellowToGreen;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        Animate();
        UpdateBar();
	}

    private void Animate() {
        Sprite[] animationSource = null;
        Image imageToUse = null;
        switch (lookDirection) {
            case LookDirection.top:
            imageToUse = moveTop;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleUp;
                break;
                case Action.move:
                animationSource = charImages.moveUp;
                break;
                default:
                break;
            }
            break;
            case LookDirection.left:
            imageToUse = moveLeft;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleSide;
                break;
                case Action.move:
                animationSource = charImages.moveSide;
                break;
                default:
                break;
            }
            break;
            case LookDirection.right:
            imageToUse = moveRight;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleSide;
                break;
                case Action.move:
                animationSource = charImages.moveSide;
                break;
                default:
                break;
            }
            break;
            case LookDirection.bottom:
            imageToUse = moveBottom;
            switch (action) {
                case Action.idle:
                animationSource = charImages.idleDown;
                break;
                case Action.move:
                animationSource = charImages.moveDown;
                break;
                default:
                break;
            }
            break;
            default:
            break;
        }
        if(imageToUse == null || animationSource == null) {
            Debug.LogError("Wrong state!");
            return;
        }

        moveTop.gameObject.SetActive(moveTop == imageToUse);
        moveBottom.gameObject.SetActive(moveBottom == imageToUse);
        moveLeft.gameObject.SetActive(moveLeft == imageToUse);
        moveRight.gameObject.SetActive(moveRight == imageToUse);


        animationTime += Time.deltaTime;
        if(animationTime > animationStayTime) {
            animationTime = 0;
            animationNr++;
        }

        if (animationNr >= animationSource.Length) {
            animationNr = 0;
        }

        imageToUse.sprite = animationSource[animationNr];


    }

    public void UpdateBar() {
        bar.fillAmount = moodToShow;
        if (moodToShow <= 0.5)
            bar.color = Color.Lerp(Color.red, Color.yellow, redToYellow.Evaluate( moodToShow * 2));
        else
            bar.color = Color.Lerp(Color.yellow, Color.green, yellowToGreen.Evaluate( 2*(moodToShow -0.5f)));


    }
}
