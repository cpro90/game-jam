﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "CharacterAnimation", menuName = "GameData/CharacterAnimation")]
public class CharacterAnimation : ScriptableObject {

    public Sprite[] idleDown, idleUp, idleSide;
    public Sprite[] moveDown, moveUp, moveSide;

}
