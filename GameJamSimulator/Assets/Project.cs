﻿using System;
public class Project {
		
	public float completed;
	public float quality;
		

	public static Project instance;

	void Awake(){
		instance = this;
	}

	public Project () {
		completed = 0;
		quality = 0.0f;
	}

	public void workOnProject (Stats stats) {

		completed += stats.motivation;
		if (completed > 1.0f)
			completed = 1.0f;

		quality += stats.creativity;
		if (quality > 1.0f)
			quality = 1.0f;
		if (quality <- 1.0f)
			quality = -1.0f;

	}
}


