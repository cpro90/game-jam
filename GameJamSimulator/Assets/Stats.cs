﻿using System;

public class Stats {

	public float motivation; //between 0 and 1
	public float creativity; //beetween -1 and 1
	public float health; // beetween 0 and 1

	public Stats (float motivation, float creativity, float health) {
		this.motivation = motivation;
		this.creativity = creativity;
		this.health = health;
	}

	public Stats copy() {
		return new Stats (motivation, creativity, health);
	}

    public static Stats Add(Stats stat1, Stats stat2) {
        return new Stats(stat1.motivation + stat2.motivation, stat1.creativity + stat2.creativity, stat1.health + stat2.health);
    }
}

