﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class AstarAI : MonoBehaviour {
    //The point to move to
    public Transform target;

    private Seeker seeker;

    //The calculated path
    public Path path;

    //The AI's speed per second
    public float speed = 2;

    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 3, lastWaypointDistance = 0.2f;

    //The waypoint we are currently moving towards
    private int currentWaypoint = 0;

    public bool goToTarget;

    public bool walking;
    public Vector3 lastDirection;

    public void WalkTo(GameObject target) {
        this.target = target.transform;
        goToTarget = true;
    }

    public void Start() {
        seeker = GetComponent<Seeker>();
    }

    public void OnPathComplete(Path p) {
        Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
        if (!p.error) {
            path = p;
            //Reset the waypoint counter
            currentWaypoint = 0;
        }
    }

    public void FixedUpdate() {
        walking = false;
        if (goToTarget) {
            goToTarget = false;
            path = null;
            seeker.StartPath(transform.position, target.position, OnPathComplete);
            return;
        }

        if (path == null) {
            //We have no path to move after yet
            return;
        }
        walking = true;

        if (currentWaypoint >= path.vectorPath.Count) {
            Debug.Log("End Of Path Reached");
            path = null;
            return;
        }

        //Direction to the next waypoint
        Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        dir *= speed * Time.fixedDeltaTime;
        dir.z = 0;
        this.gameObject.transform.Translate(dir);
        lastDirection = dir;

        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (currentWaypoint == path.vectorPath.Count - 1) {
            //last point
            if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < lastWaypointDistance) {
                currentWaypoint++;
                return;
            }
        } else {
            if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
                currentWaypoint++;
                return;
            }
        }
    }
}